#!/bin/bash

# Função para exibir mensagens
msg() {
    echo -E "/* $1 */"
}

# Exibindo variáveis configuradas
echo -e "Variables:
\\t- USER=${USER}
\\t- GROUP=${GROUP}
\\t- PROJECT=${PROJECT}
\\t- GIT_SKELETON=${GIT_SKELETON}
\\t- GIT_SKELETON_BRANCH=${GIT_SKELETON_BRANCH}
\\t- SERVERNAME=${SERVERNAME}"

cd /

# Verifica e cria o usuário se não existir
if ! getent passwd "$USER" > /dev/null; then
    msg "Create user $USER"
    useradd -m -s /bin/bash "$USER" || { echo "Failed to create user $USER"; exit 1; }
else
    msg "User $USER already exists"
fi

# Verifica e cria o grupo se não existir
if ! getent group "$GROUP" > /dev/null; then
    msg "Create group $GROUP"
    groupadd "$GROUP" || { echo "Failed to create group $GROUP"; exit 1; }
else
    msg "Group $GROUP already exists"
fi

# Verifica e remove o diretório $GIT_SKELETON se estiver vazio
if [[ -d "/$GIT_SKELETON" && -z "$( ls -A "/$GIT_SKELETON" )" ]]; then
    msg "remove empty $GIT_SKELETON"
    rm -rf /$GIT_SKELETON || { echo "Failed to remove empty $GIT_SKELETON"; exit 1; }
fi

# Clona o repositório $GIT_SKELETON se não existir
if [[ ! -d "/$GIT_SKELETON" ]]; then
    msg "Clone $GIT_SKELETON"
    git clone --branch $GIT_SKELETON_BRANCH git@bitbucket.org:m3uzz/$GIT_SKELETON.git || { echo "Failed to clone $GIT_SKELETON"; exit 1; }
    [[ "$!" -gt 0 ]] && wait $!
else
    msg "/${GIT_SKELETON} already exists"
fi

# Remove composer.lock, se presente
if [ -f "/$GIT_SKELETON/composer.lock" ] ; then 
    rm /$GIT_SKELETON/composer.lock || { echo "Failed to remove composer.lock"; exit 1; }
fi

# Cria o diretório vendor/m3uzz, se não existir
if [[ ! -d "/${GIT_SKELETON}/vendor/m3uzz" ]]; then
    mkdir /$GIT_SKELETON/vendor/m3uzz || { echo "Failed to create directory /$GIT_SKELETON/vendor/m3uzz"; exit 1; }
else
    msg "/$GIT_SKELETON/vendor/m3uzz already exists"
fi

# Função para clonar submódulos do repositório
clone_submodule() {
    local repo="git@bitbucket.org:m3uzz/onion-$1.git"
    local dir="/$GIT_SKELETON/vendor/m3uzz/onion-$1"

    if [[ ! -d "$dir" ]]; then
        git clone "$repo" "$dir" || { echo "Failed to clone $repo"; exit 1; }
    else
        msg "$dir already exists"
    fi
}

# Clonando submódulos do repositório $GIT_SKELETON/vendor/m3uzz
clone_submodule api
clone_submodule tool
clone_submodule db
clone_submodule lib
clone_submodule log
clone_submodule http
clone_submodule template

# Configura o diretório seguro para o git
msg "Setting safe directory for git"
git config --global --add safe.directory "/$GIT_SKELETON" || { echo "Failed to set safe directory for git"; exit 1; }

# Atualiza dependências do Composer
msg "Updating composer"
cd "/$GIT_SKELETON" || { echo "Failed to change directory to $GIT_SKELETON"; exit 1; }
composer update --working-dir="/$GIT_SKELETON" --no-interaction || { echo "Failed to update composer"; exit 1; }
[[ "$!" -gt 0 ]] && wait $!

if [[ ! -d "/$PROJECT/layout/vendor" ]]; then
    /$GIT_SKELETON/vendor/m3uzz/onion-tool/bin/zfstool.php ./zfstool.php -a=prepare --no-prompt --non-interactive
fi

if [[ ! -d "/$PROJECT" ]]; then
    cd /$GIT_SKELETON/client
    echo "Cloning $GIT_PROJECT"
    git clone git@bitbucket.org:m3uzz/$GIT_PROJECT.git || { echo "Failed to clone $GIT_PROJECT"; }
    [[ "$!" -gt 0 ]] && wait $!

    if [[ ! -d "/$PROJECT" ]]; then
        /$GIT_SKELETON/vendor/m3uzz/onion-tool/bin/apitool.php -a=newProject --no-prompt --non-interactive vhost=false apacherestart=false
        [[ "$!" -gt 0 ]] && wait $!
    fi
else
    msg "/$PROJECT already exists"
fi

# Configura permissões
msg "Setting permissions for $GIT_SKELETON"
chown -R "$USER:$GROUP" "/$GIT_SKELETON" || { echo "Failed to set ownership for $GIT_SKELETON"; exit 1; }
chmod -R 775 "/$GIT_SKELETON" || { echo "Failed to set permissions for $GIT_SKELETON"; exit 1; }

# Configura permissões do projeto
if [[ -d "/$PROJECT" ]]; then
    chmod -R 770 "/$PROJECT/data" || { echo "Failed to set permissions for /$PROJECT/data"; exit 1; }
    chmod -R 770 "/$PROJECT/config" || { echo "Failed to set permissions for /$PROJECT/config"; exit 1; }
fi

echo "FINISH"